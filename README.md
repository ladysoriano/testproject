**Clone Repository, Download Eclipse IDE and Add chromedriver.exe in testproject folder**

---

## Running Test

1. Right Click on testng.xml
2. Run As > TestNG Suite

---

## Accessing HTML Report

1. Go to test-output folder
2. right click emailable-report.html
3. Open With Web Browser.
