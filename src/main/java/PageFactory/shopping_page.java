package PageFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.interactions.Actions;

public class shopping_page {
	WebDriver driver;
	@FindBy(xpath="//div[@class='item-name']") List<WebElement> itemList;
	@FindBy(xpath="//div[@class='item-price']/span") List<WebElement> itemPrice;
	@FindBy(xpath="//button[@aria-label='Add to Basket']") List<WebElement> addToBasketListBtn;
	@FindBy(xpath="//button//span[text()=' Your Basket']") WebElement basketNavBtn;
	@FindBy(xpath="//snack-bar-container//button") WebElement closeSnackBarBtn;
	@FindBy(xpath="//button//span[text()=' Your Basket']/following-sibling::span") WebElement basketItemCount;
		
	public shopping_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//get Item Name Index
	public Integer getItemIndex(String strItemName) {
		List<String> strItems = new ArrayList<String>();
		for (WebElement item : itemList) {
				strItems.add(item.getText());
	        }
		Integer intIndex = strItems.indexOf(strItemName); 
		return intIndex;
	}
	
	//get item price using index
	public Double getItemPrice(Integer intIndex) {
		String strItemPrice = itemPrice.get(intIndex).getText();
		Double dblItemPrice = Double.parseDouble(StringUtils.chop(strItemPrice));
		return dblItemPrice;
	}
	
	//click Add To Basket Button using index
	public void clickAddToBasket(Integer index) {
		Actions action = new Actions(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		action.moveToElement(addToBasketListBtn.get(index)).perform();
		addToBasketListBtn.get(index).click();
		this.clickCloseSnackBarBtn();
	}
	
	public void clickCloseSnackBarBtn() {
		closeSnackBarBtn.click();
	}
	
	//get total count of items in basket
	public Integer getItemCountInBasket() {
		Actions action = new Actions(driver);
		action.moveToElement(basketItemCount);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		String strItemCount = basketItemCount.getText();
		Integer intItemCount = Integer.parseInt(strItemCount);
		return intItemCount;
	}
	
	//click Basket button
	public void clickBasketBtn() {
		Actions action = new Actions(driver);
		action.moveToElement(basketNavBtn).perform();
		basketNavBtn.click();
	}
	
	//get total price of items added
	public String getTotalPrice(ArrayList<Double> dblPrices) {
		Double totalPrice = 0.00;
		for(int i = 0; i < dblPrices.size(); i++) {
			totalPrice = totalPrice + dblPrices.get(i);
		}
		String strTotalPrice = String.format("%.2f", totalPrice);
		return strTotalPrice;
	}
	
	//add items to cart
	public String addItemsToCart(String[] strItems) {
		Integer intItemCount = strItems.length;
				
		ArrayList<Double> dblPrice = new ArrayList<Double>();
		for(int i = 0; i < intItemCount; i++) {
			Integer btnIndex = this.getItemIndex(strItems[i]);
			dblPrice.add(this.getItemPrice(btnIndex));
			this.clickAddToBasket(btnIndex);
		}
		String strComputedTotalPrice = this.getTotalPrice(dblPrice);
		return strComputedTotalPrice;
	}

}
