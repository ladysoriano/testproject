package PageFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class order_review_page {
	WebDriver driver;
	@FindBy(xpath="//h1[text()='Thank you for your purchase!']") WebElement purchaseHeader;
	@FindBy(xpath="//div/b[text()='Delivery Address']/parent::div//following-sibling::div") List<WebElement> deliveryDetailsLbl;
	@FindBy(xpath="//mat-row/mat-cell[1]") List<WebElement> itemsAdded;
	@FindBy(xpath="//table[@class=\"price-align\"]/tr[1]/td") WebElement itemTotalPriceLbl;
	@FindBy(xpath="//table[@class=\"price-align\"]/tr[2]/td") WebElement deliveryFeeLbl;
	@FindBy(xpath="//table[@class=\"price-align\"]/tr[4]/td") WebElement totalPriceLbl;
	@FindBy(id="checkoutButton") WebElement checkoutBtn;
	
	public order_review_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//get delivery details
	public List<String> getDeliveryDetails() {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(purchaseHeader));
		String[] deliveryDetails = new String[deliveryDetailsLbl.size()];
		for (int i = 0; i < deliveryDetailsLbl.size(); i++) {
			deliveryDetails[i] = deliveryDetailsLbl.get(i).getText();
		}

		List<String> strDeliveryDetails = new ArrayList<String>();
		strDeliveryDetails = Arrays.asList(deliveryDetails);
		return strDeliveryDetails;
	}
	
	//get all items in the basket
	public List<String> getItemsInTheBasket() {
		String[] itemDetails = new String[itemsAdded.size()];
		for (int i = 0; i < itemsAdded.size(); i++) {
			itemDetails[i] = itemsAdded.get(i).getText();
		}

		List<String> strItemDetails = new ArrayList<String>();
		strItemDetails = Arrays.asList(itemDetails);
		return strItemDetails;
	}
	
	//get Items Total Price
	public String getItemTotalPrice() {
		String strItemTotalPrice = itemTotalPriceLbl.getText();
		strItemTotalPrice = StringUtils.chop(strItemTotalPrice);
		return strItemTotalPrice;
	}
	
	//get Delivery Fee
	public String getDeliveryFee() {
		String strDeliveryFee = deliveryFeeLbl.getText();
		strDeliveryFee = StringUtils.chop(strDeliveryFee);
		return strDeliveryFee;
	}
	
	//get total price
	public String getTotalPrice() {
		String strTotalPrice = totalPriceLbl.getText();
		strTotalPrice = StringUtils.chop(strTotalPrice);
		return strTotalPrice;		
	}
	
	//compute total price
	public String computeTotalPrice(String strComputedItemTotalPrice, String strDeliveryPrice) {
		Double dblItemTotal = Double.parseDouble(strComputedItemTotalPrice);
		Double dblDeliveryFee = Double.parseDouble(strDeliveryPrice);
		Double dblTotalPrice = dblItemTotal + dblDeliveryFee;
		String strTotalPrice = String.valueOf(dblTotalPrice);
		return strTotalPrice;
	}
	
	//click checkout button
	public void clickCheckoutBtn() {
		checkoutBtn.click();
	}
}
