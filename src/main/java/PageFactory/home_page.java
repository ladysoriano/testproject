package PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class home_page {
	WebDriver driver;
	@FindBy(xpath="//button[@aria-label='Back to homepage']") WebElement homepageHeader;
	@FindBy(xpath="//button[@aria-label='Close Welcome Banner']") WebElement closeBannerBtn;
	@FindBy(xpath="//a[@aria-label='dismiss cookie message']") WebElement closeMessageBtn;
	@FindBy(id="navbarAccount") WebElement userAccountNavBtn;
	@FindBy(id="navbarLoginButton") WebElement loginNavBtn;
	@FindBy(id="navbarLogoutButton") WebElement logoutNavBtn;
	
	public home_page (WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//get title of home page
	public String getHomePageTitle() {
		return homepageHeader.getText();
	}
	
	//close Welcome Banner
	public void clickCloseBannerBtn() {
		closeBannerBtn.click();
	}
	
	//close Cookie Message
	public void clickCloseCookieBtn() {
		closeMessageBtn.click();
	}
	
	//click Account Navigation
	public void clickAccountNav() {
		userAccountNavBtn.click();
	}
	
	//click Login Navigation
	public void clickLoginNav() {
		loginNavBtn.click();
	}
	
	//click Logout Navigation
	public void clickLogoutNav() {
		logoutNavBtn.click();
	}
	
	//navigate to login
	public void navigateToLogin() {
		this.clickCloseBannerBtn();
		this.clickCloseCookieBtn();
		this.clickAccountNav();
		this.clickLoginNav();
	}
	
	//logout from the application
	public void logoutFromTheApplication() {
		this.clickAccountNav();
		this.clickLogoutNav();
	}
}
