package PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class login_page {
	WebDriver driver;
	@FindBy(xpath="//mat-card/h1") WebElement loginHeader;
	@FindBy(name="email") WebElement emailTxt;
	@FindBy(name="password") WebElement passwordTxt;
	@FindBy(id="loginButton") WebElement loginBtn;
	
	public login_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//get Login page header
	public String getLoginHeader() {
		return loginHeader.getText();
	}
	
	//set email address in textbox
	public void setEmailAddress(String strEmailAddress) {
		emailTxt.sendKeys(strEmailAddress);
	}
	
	//set password in textbox
	public void setPassword(String strPassword) {
		passwordTxt.sendKeys(strPassword);
	}
	
	//click Login button
	public void clickLoginButton() {
		loginBtn.click();
	}
	
	//login to the system
	public void loginToShop(String strEmailAddress, String strPassword) {
		this.setEmailAddress(strEmailAddress);
		this.setPassword(strPassword);
		this.clickLoginButton();
	}
}
