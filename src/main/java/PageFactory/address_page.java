package PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class address_page {
	WebDriver driver;
	@FindBy(xpath="//h1[text()='Add New Address']") WebElement addNewAddressHeader;
	@FindBy(xpath="//input[contains(@placeholder, 'country')]") WebElement countryTxt;
	@FindBy(xpath="//input[contains(@placeholder, 'name')]") WebElement nameTxt;
	@FindBy(xpath="//input[contains(@placeholder, 'mobile number')]") WebElement mobileNumberTxt;
	@FindBy(xpath="//input[contains(@placeholder, 'ZIP code')]") WebElement zipCodeTxt;
	@FindBy(xpath="//textarea[contains(@placeholder, 'address')]") WebElement addressTxt;
	@FindBy(xpath="//input[contains(@placeholder, 'city')]") WebElement cityTxt;
	@FindBy(xpath="//input[contains(@placeholder, 'state')]") WebElement stateTxt;
	@FindBy(id="submitButton") WebElement submitBtn;
	
	public address_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//add new address
	public void fillUpNewAddress(String[] strAddress) {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(addNewAddressHeader));
		countryTxt.sendKeys(strAddress[0]);
		nameTxt.sendKeys(strAddress[1]);
		mobileNumberTxt.sendKeys(strAddress[2]);
		zipCodeTxt.sendKeys(strAddress[3]);
		addressTxt.sendKeys(strAddress[4]);
		cityTxt.sendKeys(strAddress[5]);
		stateTxt.sendKeys(strAddress[6]);
		this.clickSubmitButton();
	}
	
	//click submit button
	public void clickSubmitButton() {
		submitBtn.click();
	}
}
