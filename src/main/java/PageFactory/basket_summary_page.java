package PageFactory;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import PageFactory.address_page;
import PageFactory.shopping_page;

public class basket_summary_page {
	WebDriver driver;
	address_page objAddressPage;
	shopping_page objShoppingPage;
	@FindBy(xpath="//mat-card//h1/small") WebElement basketUserAcctLbl;
	@FindBy(id="checkoutButton") WebElement checkoutBtn;
	@FindBy(xpath="//button[@aria-label='Add a new address']") WebElement addNewAddressBtn;
	@FindBy(xpath="//h1[text()='Select an address']") WebElement selectAddressHeader;
	@FindBy(xpath="//mat-row") List<WebElement> addressRows;
	@FindBy(xpath="//mat-row/mat-cell[2]") List<WebElement> tableNameColumnList;
	@FindBy(xpath="//mat-row/mat-cell[1]/mat-radio-button") List<WebElement> tableRdoBtn;
	@FindBy(xpath="//mat-row/mat-cell[3]") List<WebElement> deliveryTblPrice;
	@FindBy(xpath="//button[contains(@aria-label,'Proceed to') and contains(@aria-label, 'selection')]") WebElement continueBtn;
	
	public basket_summary_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//get user account
	public String getbasketUserAccount() {
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(basketUserAcctLbl));
		return basketUserAcctLbl.getText();
	}
	
	//click Checkout button
	public void clickCheckoutBtn() {
		checkoutBtn.click();
	}
	
	//click Add New Address
	public String[] clickAddNewAddress(String strAddressDetails) {
		String[] strAddressDetail = strAddressDetails.split(", ");
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(selectAddressHeader));
		if(addressRows.size() == 0) {
			addNewAddressBtn.click();
			objAddressPage = new address_page(driver);
			objAddressPage.fillUpNewAddress(strAddressDetail);
			objShoppingPage = new shopping_page(driver);
			objShoppingPage.clickCloseSnackBarBtn();
		}
		return strAddressDetail;
	}
	
	//Select an address/delivery speed based on name
	public Integer selectFromTable(String strNameSelection) {
		List<String> strNames = new ArrayList<String>();
		for (WebElement nameColumn : tableNameColumnList) {
			strNames.add(nameColumn.getText());
	        }
		Integer intIndex = strNames.indexOf(strNameSelection); 
		tableRdoBtn.get(intIndex).click();
		return intIndex;
	}
	
	//Click proceed to payment button
	public void clickContinueBtn() {
		continueBtn.click();
	}
	
	//get delivery price
	public String getDeliveryPrice(Integer intIndex) {
		String strDelPrice = deliveryTblPrice.get(intIndex).getText();
		strDelPrice = StringUtils.chop(strDelPrice);
		return strDelPrice;
	}
	
	//select address and continue
	public List<String> selectAddressAndContinueToPayment(String strAddressDetails) {
		String[] strAddressName = this.clickAddNewAddress(strAddressDetails);
		this.selectFromTable(strAddressName[1]);
		this.clickContinueBtn();
		List<String> strAddress = new ArrayList<String>();
		strAddress.add(strAddressName[0]);
		strAddress.add(strAddressName[1]);
		strAddress.add("Phone Number " + strAddressName[2]);
		strAddress.add(strAddressName[4] + ", " + strAddressName[5] + ", " + strAddressName[6] + ", " + strAddressName[3]);
		return strAddress;
	}
	
	//select delivery speed and continue
	public String selectDeliverySpeedAndContinue(String strDeliverySpeed) {
		Integer intDeliveryIndex = this.selectFromTable(strDeliverySpeed);
		String strDeliveryPrice = this.getDeliveryPrice(intDeliveryIndex);
		this.clickContinueBtn();
		return strDeliveryPrice;
	}
}
