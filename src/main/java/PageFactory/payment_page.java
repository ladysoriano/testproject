package PageFactory;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;

public class payment_page {
	WebDriver driver;
	@FindBy(xpath="//h1[text()='My Payment Options']") WebElement paymentOptionsHeader;
	@FindBy(xpath="//mat-row") List<WebElement> paymentRows;
	@FindBy(xpath="//mat-expansion-panel-header/span/mat-panel-title[text()=' Add new card ']") WebElement addCreditCardPaymentTab;
	@FindBy(xpath="//mat-label[text()= 'Name']//parent::label/parent::span/parent::div/input") WebElement cardHolderNameTxt;
	@FindBy(xpath="//mat-label[text()= 'Card Number']//parent::label/parent::span/parent::div/input") WebElement cardNumberTxt;
	@FindBy(xpath="//mat-label[text()= 'Expiry Month']//parent::label/parent::span/parent::div/select") WebElement expiryMonthDD;
	@FindBy(xpath="//mat-label[text()= 'Expiry Year']//parent::label/parent::span/parent::div/select") WebElement expiryYearDD;
	@FindBy(id="submitButton") WebElement submitBtn;
	@FindBy(xpath="//mat-row/mat-cell[3]") List<WebElement> tableNameColumnList;
	@FindBy(xpath="//mat-row/mat-cell[1]/mat-radio-button") List<WebElement> tableRdoBtn;
	@FindBy(xpath="//button[contains(@aria-label,'Proceed to review')]") WebElement continueBtn;
	
	public payment_page(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	//select payment option
	public String[] clickAddNewCreditCard(String strCardDetails) {
		String[] strCardDetail = strCardDetails.split(", ");
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(paymentOptionsHeader));
		if(paymentRows.size() == 0) {
			addCreditCardPaymentTab.click();
			this.addNewCreditCard(strCardDetail);
		}
		return strCardDetail;
	}
	
	//add new credit card
	public void addNewCreditCard(String[] strCardDetail) {
		cardHolderNameTxt.sendKeys(strCardDetail[0]);
		cardNumberTxt.sendKeys(strCardDetail[1]);
		Select expiryMonthDropdown = new Select(expiryMonthDD);
		expiryMonthDropdown.selectByVisibleText(strCardDetail[2]);
		Select expiryYearDropdown = new Select(expiryYearDD);
		expiryYearDropdown.selectByVisibleText(strCardDetail[3]);
		this.clickSubmitButton();
	}
	
	//click submit button
	public void clickSubmitButton() {
		submitBtn.click();
	}
	
	//select payment option
	public void selectPaymentOption(String strCardHolderName) {
		List<String> strNames = new ArrayList<String>();
		for (WebElement nameColumn : tableNameColumnList) {
			strNames.add(nameColumn.getText());
	        }
		Integer intIndex = strNames.indexOf(strCardHolderName); 
		tableRdoBtn.get(intIndex).click();
	}
	
	//click Continue Button
	public void clickContinueBtn() {
		continueBtn.click();
	}
	
	//select payment option and continue
	public List<String> selectPaymentOptionAndContinue(String strCardDetails) {
		String[] strCardDetail = this.clickAddNewCreditCard(strCardDetails);
		this.selectPaymentOption(strCardDetail[0]);
		this.clickContinueBtn();

		List<String> strPaymentDetails = new ArrayList<String>();
		strPaymentDetails.add("Card Holder " + strCardDetail[0]);
		String lastFourDigits = strCardDetail[1].substring(strCardDetail[1].length() - 4);
		strPaymentDetails.add("Card ending in " + lastFourDigits);
		return strPaymentDetails;
	}
}
