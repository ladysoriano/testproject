package PlaceOrder;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.Arrays;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import PageFactory.shopping_page;
import PageFactory.basket_summary_page;
import PageFactory.home_page;
import PageFactory.login_page;
import PageFactory.payment_page;
import PageFactory.order_review_page;

public class placeOrder {
	String driverPath = ".\\chromedriver.exe";
	WebDriver driver;
	home_page objHomePage;
	login_page objLoginPage;
	shopping_page objShoppingPage;
	basket_summary_page objBasketSummaryPage;
	payment_page objPaymentPage;
	order_review_page objOrderReviewPage;
	String strEmailAddress = "testacc@test.com";
	String strPassword = "test1234";
	String strItemName = "Eggfruit Juice (500ml), Apple Pomace, Green Smoothie, Fruit Press";
	String strAddressDetails = "Philippines, home, 9999999999, 1101, test Address, Quezon, Metro Manila";
	String strDeliverySpeed = " Fast Delivery";
	String strCardDetails = "test, 6331101999990016, 12, 2083";
	
	@BeforeTest
	public void setup(){
        System.setProperty("webdriver.chrome.driver", driverPath);        
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://test.cuongnguyen.online/");
        driver.manage().window().maximize();
    }
	
	@Test
	public void test_Successfully_Place_Order() {	
		String[] strItems = strItemName.split(", ");
		List<String> strItemsList = new ArrayList<String>();
		strItemsList = Arrays.asList(strItems);
		Integer intItemCount = strItems.length;
		
		//Navigate to Login
		objHomePage = new home_page(driver);
		String homePageHeader = objHomePage.getHomePageTitle();
		AssertJUnit.assertTrue("Home page is displayed", homePageHeader.toLowerCase().contains("owasp juice shop"));
		objHomePage.navigateToLogin();
		
		//Login to application
		objLoginPage = new login_page(driver);
		String loginPageHeader = objLoginPage.getLoginHeader();
		AssertJUnit.assertTrue("Login page is displayed", loginPageHeader.toLowerCase().contains("login"));
		objLoginPage.loginToShop(strEmailAddress, strPassword);
		
		//Add Items to Cart
		objShoppingPage = new shopping_page(driver);
		String strComputedItemTotalPrice = objShoppingPage.addItemsToCart(strItems);		
		
		Integer itemCount = objShoppingPage.getItemCountInBasket();
		AssertJUnit.assertTrue("Successfully added all items", itemCount.equals(intItemCount));	
		objShoppingPage.clickBasketBtn();
		
		//Validate if account is correct
		objBasketSummaryPage = new basket_summary_page(driver);
		String userAcct = objBasketSummaryPage.getbasketUserAccount();
		AssertJUnit.assertTrue("Basket for "+ strEmailAddress + " is displayed", userAcct.toLowerCase().contains(strEmailAddress));
		
		//Checkout and select address
		objBasketSummaryPage.clickCheckoutBtn();
		List<String> strAddress = objBasketSummaryPage.selectAddressAndContinueToPayment(strAddressDetails);
		
		//select delivery speed and continue
		String strDeliveryPrice = objBasketSummaryPage.selectDeliverySpeedAndContinue(strDeliverySpeed);
		
		//select payment option and continue
		objPaymentPage = new payment_page(driver);
		objPaymentPage.selectPaymentOptionAndContinue(strCardDetails);
		
		//proceed to checkout
		objOrderReviewPage = new order_review_page(driver);
		objOrderReviewPage.clickCheckoutBtn();
		
		//Validate if all details is correct
		//Validate delivery details
		List<String> strDeliveryDetails = objOrderReviewPage.getDeliveryDetails();
		AssertJUnit.assertTrue("Address Details is correct", strDeliveryDetails.containsAll(strAddress));
		
		//Validate basket details
		List<String> strItemDetails = objOrderReviewPage.getItemsInTheBasket();
		AssertJUnit.assertTrue("Item Details is correct", strItemDetails.containsAll(strItemsList));
		
		//Validate Total Price of Items
		String strItemTotalPrice = objOrderReviewPage.getItemTotalPrice();
		AssertJUnit.assertTrue("Item Total Price is correct", strItemTotalPrice.contains(strComputedItemTotalPrice));
		
		//Validate Delivery fee
		String strDeliveryFee = objOrderReviewPage.getDeliveryFee();
		AssertJUnit.assertTrue("Delivery Fee is correct", strDeliveryFee.contains(strDeliveryPrice));
		
		//Validate Total Price
		String strTotalPrice = objOrderReviewPage.getTotalPrice();
		String strComputedTotalPrice = objOrderReviewPage.computeTotalPrice(strComputedItemTotalPrice, strDeliveryPrice);
		AssertJUnit.assertTrue("Total Price is correct", strTotalPrice.contains(strComputedTotalPrice));
	}
	
	@AfterTest
	public void teardown(){
        objHomePage.logoutFromTheApplication();
        driver.quit();
    }
	
}
